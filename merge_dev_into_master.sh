#!/bin/sh

###################################################################
#                                                                 #
# Merge `development` branch into `master` branch and push with a #
# generic but explicit tag.                                       #
#                                                                 #
# The tag is update depending on the previous one:                #
#   - 1 -> 2                                                      #
#   - 1.0 -> 1.1                                                  #
#   - 1.0.1 -> 1.0.2                                              #
#                                                                 #
# The description of the tag has the following format:            #
#   Release <tag> - MM.DD.YY                                      #
#                                                                 #
###################################################################

tag=`git describe --tags --abbrev=0`
subversion=$((`echo $tag | sed "s/.*\.//"` + 1))
tag=`echo $tag | sed "s/\.[^.]*$/\.$subversion/"`
date=`date +"%m.%d.%y"`

git pull origin master
git checkout master
git merge development
git tag -a $tag -m "Release $tag - $date"
git push --tags origin
