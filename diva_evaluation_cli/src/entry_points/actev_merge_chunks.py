"""Entry point module: merge-chunks

Implements the entry-point by using Python or any other languages.
"""

import argparse
import json


def entry_point(result_location, output_file, chunks_file, chunk_ids,
                system_cache_dir):
    """Method to complete: you have to raise an exception if an error occured
    in the program.

    Given a list of chunk ids, merges all the chunks system output present in
    the list.

    Args:
        result_location (str): Path to get the result of the chunks processing
        chunk_file (str):  Path to generate a chunk file, that summarizes all
            the processed videos/activities
        output_file (str): Path to the output file generated
        chunk_ids (:obj:`list`): List of chunk ids
        system_cache_dir (str): Path to system cache directory

    """
    raise NotImplementedError("You should implement the entry_point method.")
