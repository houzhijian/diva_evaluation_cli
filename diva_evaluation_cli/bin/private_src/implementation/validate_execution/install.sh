#!/bin/bash
env_dir="python_env"
current_path=`realpath $(dirname $0)`
path_to_env_dir="$current_path/$env_dir"

if [ -d $path_to_env_dir ];then
  . $path_to_env_dir/bin/activate
else
  sudo apt-get install python3-dev -y
  virtualenv -p python3 $path_to_env_dir
  . $path_to_env_dir/bin/activate
  python3 -m pip --no-cache-dir install -r $current_path/requirements.txt
fi
