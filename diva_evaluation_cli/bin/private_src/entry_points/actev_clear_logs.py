"""Entry point module: exec

This file should not be modified.
"""
import os


def entry_point():
    """Private entry point.

    Delete temporary files as resource and status monitoring files.

    """
    # go into the right directory to execute the script
    path = os.path.dirname(__file__)
    script = os.path.join(path, '../implementation/clear_logs/clear_logs.sh')

    # status is a 16-bit long integer
    # The 8 strongest bits are exit_code
    # The 8 weakest bits are signal_number
    status = os.system(script)
    exit_code = status >> 8
    signal_number = status & (2**8 - 1)
    if status != 0:
        if signal_number == 2:  # SIGINT
            raise KeyboardInterrupt
        else:
            raise Exception("Error occured in clear_logs.sh")
