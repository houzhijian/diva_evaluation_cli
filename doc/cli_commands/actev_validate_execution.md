# actev validate-execution

## Description

Test the execution using the ActEV scorer on each validation data set provided in container_output directory.
More information about the ActEV scorer at: https://github.com/usnistgov/ActEV_Scorer/

This command requires the following parameters:

## Parameters

| Name      | Id         | Required | Definition                 |
|-----------|------------|---------|----------------------------|
| output         | o | True    | path to experiment output json file    |
| reference      | r | False   | path to reference json file            |
| file-index     | f | True    | path to file index json file           |
| activity-index | a | True    | path to activity index json file       |
| result         | R | False   | path to result of the ActEV scorer     |   
| score          | s | False   | sets flag to score the system output against a reference |

## Usage

Generic command:

```
actev validate-execution -o <path to output result> -r <path to reference file> -a <path to activity> -f <path to file> -R <path to scoring result>
```

Example:

```
actev validate-execution ~/output.json -r diva_evaluation_cli/container_output/dataset/output.json -a ~/activity.json -f ~/file.json -R ~/result.json --score

```


